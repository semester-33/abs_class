package sandwiches;

import static org.junit.Assert.*;

import org.junit.Test;

public class BLTSandwichTest {

    @Test
    public void getFillingTest()
    {
        BLTSandwich blt = new BLTSandwich();

        assertEquals("Bacon, lettuce, tomato", blt.getFilling());


    }

    @Test
    public void addFillingTest()
    {
        BLTSandwich blt = new BLTSandwich();
        blt.addFilling("cheese");
        assertEquals("bacon, lettuce, tomato, cheese", blt.getFilling());
    }

    @Test
    public void isVegetarianTest()
    {
        BLTSandwich blt = new BLTSandwich();
        assertEquals(false, blt.isVegetarian());
    }
}
