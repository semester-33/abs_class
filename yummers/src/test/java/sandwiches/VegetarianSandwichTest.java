package sandwiches;

import static org.junit.Assert.*;

import org.junit.Test;

public class VegetarianSandwichTest {
    
    @Test 
    public void check_constructor_good(){
        VegetarianSandwich vs = new CucumberSandwich();
        VegetarianSandwich vs2 = new Tofu();

        assertEquals("cucumber", vs.getFilling());
        assertEquals("Tofu", vs2.getFilling());
    }

    @Test
    public void expect_filling_to_add(){
        VegetarianSandwich vs = new CucumberSandwich();
        VegetarianSandwich vs2 = new Tofu();
         vs.addFilling("Lettuce");
         vs2.addFilling("Lettuce");
         assertEquals("cucumber, Lettuce", vs.getFilling());
         assertEquals("Tofu, Lettuce", vs2.getFilling());
        
    }

    @Test
    public void check_if_veg(){
        VegetarianSandwich vs = new CucumberSandwich();
        VegetarianSandwich vs2 = new Tofu();

        System.out.println(vs.isVegetarian());
        System.out.println(vs2.isVegetarian());
    }
}
