package sandwiches;

public class BLTSandwich implements ISandwich{
    
    private String filling;
    
    public BLTSandwich(){
        this.filling = "bacon, lettuce, tomato";
    }

    @Override
    public String getFilling(){
        return this.filling;
    }

    @Override
    public void addFilling(String topping){
        this.filling += ", " + topping;
    }

    @Override
    public boolean isVegetarian(){
        return false;
    }
}
