package sandwiches;

public class Tofu extends VegetarianSandwich {

    public Tofu() {
        super.addFilling("Tofu");
    }

    @Override
    public String getProtein() {
        return "Tofu";
    }
    
    public String getFilling() {
        return super.getFilling();
    }
    
}
