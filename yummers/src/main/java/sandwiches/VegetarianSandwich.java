package sandwiches;

public abstract class VegetarianSandwich implements ISandwich {

    private String filling;

    public VegetarianSandwich() {
        this.filling = "";
    }

    public String getFilling() {
        return this.filling;
    }

    public abstract String getProtein();

    @Override
    public void addFilling(String topping){
        String[] badFood = {"chicken", "beef", "fish", "meat", "pork"};
        for(int i = 0 ; i < badFood.length; i++) {
            if(topping.equals(badFood[i])) {
                throw new IllegalArgumentException("BAD FOOD");
            }
        }

        if(this.filling.equals("")){
            this.filling += topping;
        } else {
            this.filling += ", " + topping;
        }
    }

    public boolean isVegetarian(){
        return true;
    }

    public boolean isVegan(){
        String[] fillings = this.filling.split(", ");
        
        for(int i = 0; i < fillings.length; i++){
            if(fillings[i].equals("egg") || fillings[i].equals("cheese")){
                return false;
            }
        }
        return true;
    }
    
}
