package sandwiches;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        ISandwich sand = new Tofu();
        Tofu tofu = new Tofu();

        if(tofu instanceof ISandwich){
            System.out.println("yes");
        }

       // tofu.addFilling("chicken");

       System.out.println(((VegetarianSandwich)sand).isVegan());

       VegetarianSandwich veg = new CucumberSandwich();
       System.out.println(veg.isVegetarian());
    }
}
