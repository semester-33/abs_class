package sandwiches;

public class CucumberSandwich extends VegetarianSandwich{
    
    public CucumberSandwich(){
        super.addFilling("cucumber");
    } 

    public String getFilling(){
        return super.getFilling();
    }

    @Override
    public String getProtein(){
        throw new UnsupportedOperationException("Haven't written it yet");
    }
}
